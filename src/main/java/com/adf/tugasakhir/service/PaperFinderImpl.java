package com.adf.tugasakhir.service;

import java.util.List;

import com.adf.tugasakhir.dataclass.Document;
import com.adf.tugasakhir.dataclass.Documents;
import com.adf.tugasakhir.dataclass.KeyPhrasesResponseDocument;
import com.adf.tugasakhir.dataclass.KeyPhrasesResult;
import com.adf.tugasakhir.dataclass.PaperResult;
import com.adf.tugasakhir.model.Conference;
import com.adf.tugasakhir.service.supplier.ScienceDirectPaperFinder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PaperFinderImpl
 */
@Service
public class PaperFinderImpl implements PaperFinderService {

    @Autowired
    TextAnalysisService textAnalysisService;

    @Override
    public PaperResult findPaperFromConference(Conference conference) {
        String query = getQueriesFromConference(conference);

        // Setup the documents wrapper
        Document docWrapper = new Document("", "en", query);
        Documents docs = new Documents();
        docs.addDocument(docWrapper);

        // For demonstration purposes, this is not wrapped into a function
        // To show that yes, the API is kind of jumbled but simple in nature
        KeyPhrasesResult extractResult = textAnalysisService.extractKeyPhrases(docs);
        KeyPhrasesResponseDocument  docResult = extractResult.getDocuments().get(0); // Because there's only one document that's being wrapped
        
        // Find the papers
        List<String> keyPhrases = docResult.getKeyPhrases();
        PaperResult pResult = ScienceDirectPaperFinder.getInstance().findPapers(condenseString(keyPhrases));
        return pResult;
    }

    private String getQueriesFromConference(Conference conference) {
        return conference.getNama();
    }

    private String condenseString(List<String> strings) {
        StringBuilder sb = new StringBuilder();
        for (String s : strings) {
            sb.append(s);
            sb.append(" ");
        }
        return sb.toString();
    }

}