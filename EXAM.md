# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

Inez Zahra Nabila - 1806241091

Work Checklist



URL to your GitLab project (make sure it is public!)
//TODO: Write the answers related to the programming exam here.

TASK 1
1. the APIKEY has not yet been secured and can still be seen in application.properties. To fix this we can take the API KEY written and add it to the configuration -> environment variables. We then need to change the application properties' API KEY to ${TEXT_ANALYTICS_API_KEY}.
2. (config) The text analytics final exam has not yet been added to the environment. This is because it is an external credentials that we need such as lombok (task 3) and prometheus (task 2) therefore we need to change the dependency in build.gradle and configurations.


TASK 2
![prom1](images/prom1.png)
![prom2](images/prom2.png)
did the prometheus

TASK 3
implemented lombok, used it for the Conference class and modified the Conference class.

TASK 4
Changed the Paper class and stored it.

TASK 5